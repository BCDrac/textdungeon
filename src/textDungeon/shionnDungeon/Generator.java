package textDungeon.shionnDungeon;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class Generator {
	/** Classe générant un donjon
	* @author Shionn (http://www.shionn.org/)
	*/
	
	private enum DIR {
		N(1, 0, -1), S(2, 0, 1), E(4, 1, 0), W(8, -1, 0);
		private final int bit;
		private final int dx;
		private final int dy;
		private DIR opposite;
 
		// use the static initializer to resolve forward references
		static {
			N.opposite = S;
			S.opposite = N;
			E.opposite = W;
			W.opposite = E;
		}
		
		private DIR(int bit, int dx, int dy) {
			this.bit = bit;
			this.dx = dx;
			this.dy = dy;
		}
	};

	private static final int MIN_ROOM_SIZE = 3;
	private static final int MIN = 23;
	private static final int MAX = 24;
	private List <Zone> roomsList = new ArrayList<Zone>();
	private List <Zone> monsterRooms = new ArrayList<Zone>();
	private LinkedList <Zone> linkedRooms;
	private int spawnPointX;
	private int spawnPointY;
	private int end;
	//static ResourceBundle rb = ResourceBundle.getBundle("textDungeon.config");
	 
	/**
	 * Créé un donjon avec éléments aléatoires
	 * @param seed : valeur aléatoire infulant sur des aspects du donjon
	 * @return dungeon : renvoie le donjon créé pour être utilisable par d'autres méthodes
	 */
	public Dungeon generate(Random seed) {
		Dungeon dungeon = new Dungeon(48,24);
		//Dungeon dungeon = new Dungeon(nextInt(seed, MIN, MAX), nextInt(seed, MIN, MAX));
		//Zone zone = new Zone (int startx, int endx, int starty, int endy)
		//Basically, zone has the size of the dungeon
		Zone zone = new Zone(0, dungeon.getWidth() - 1, 0, dungeon.getHeight() - 1);
		fill(dungeon, zone, MapElt.WALL); //Fill the dungeon with MapElt.WALL (solid)
		generate(dungeon, zone, seed);
		findClosedRooms(dungeon);
	/*	for(Zone room : roomsList) {
			 int xx = room.getStartX() + 2;
			 int yy = room.getStartY() + 2;

			// if (!dungeon.get(xx, yy).isSolid()) {
				 dungeon.setNumb(xx, yy, MapElt.MONSTER);
			 //}
		}
		//spawnPlayer(dungeon, zone);
	    //spawnMonsters(dungeon, seed);
	    //spawnItems(dungeon, zone, seed);
		//spawnExit(dungeon, zone);
		System.out.println(roomsList.get(0).getId());
		System.out.println(roomsList.get(1).getId());
		System.out.println(roomsList.get(2).getId());
		if (roomsList.get(1).getId() == roomsList.get(2).getId()) {
			dungeon.set(roomsList.get(0).getStartX() + 1, roomsList.get(1).getStartY() + 1, MapElt.MISSING);
			dungeon.set(roomsList.get(1).getStartX() + 1, roomsList.get(1).getStartY() + 1, MapElt.ONE);
			dungeon.set(roomsList.get(2).getStartX() + 1, roomsList.get(2).getStartY() + 1, MapElt.TWO);
		}*/
		return dungeon;
	}
	/*
	public Dungeon paramGen(Random seed) {
		int x = Integer.parseInt(rb.getString("donjon.x"));
		int y = Integer.parseInt(rb.getString("donjon.y"));
		Dungeon dungeon = new Dungeon(x, y);
		Zone zone = new Zone(0, dungeon.getWidth() - 1, 0, dungeon.getHeight() - 1);
		fill(dungeon, zone, MapElt.WALL);
		generate(dungeon, zone, seed);
		spawnPlayer(dungeon, zone);
		return dungeon;
	}*/
	
	  
  /** 
   * Permet de placer le joueur sur la map de façon aléatoire
   * @author Cécile
   */
	private void spawnPlayer(Dungeon dungeon, Zone zone) {
				dungeon.set(1, 1, MapElt.PLAYER);
	}
	
	/**
	 * Permet de placer la sortie de façon aléatoire
	 * @param dungeon : le donjon qui sera utilisé dans la méthode
	 * @param zone : la "zone de jeu" qui sera construite
	 */
	private void spawnExit(Dungeon dungeon, Zone zone) {
		/*
		int oppositeX = 0;
		int oppositeY = 0;
		if (dungeon.getWidth() / 2 > spawnPointX && dungeon.getHeight() / 2 > spawnPointY) {
			oppositeX = zone.getEndX() - spawnPointX ;
			oppositeY = zone.getEndY() - spawnPointY;
			
		}else if (dungeon.getWidth() / 2 < spawnPointX && dungeon.getHeight() / 2 < spawnPointY) {
			oppositeX = zone.getStartX() + spawnPointX - 1;
			oppositeY = zone.getStartY() + spawnPointY - 1;
			
		}
		//findLongestPath();
		dungeon.set(oppositeX, oppositeY, MapElt.EXIT);
		zone.setExit(true);
		}*/
		boolean exitExists = false;
		int x = 0;
		int y = 0;
		while (!exitExists) {
			x = random(zone.getStartX() + 1, zone.getEndX() - 1);
			y = random(zone.getStartY() + 1, zone.getEndY() - 1);
			if (!dungeon.get(x, y).isSolid()) {	  
				dungeon.set(x, y, MapElt.PLAYER);
				exitExists = true;
			}
		}
		dungeon.set(x, y, MapElt.EXIT);
	}
	
	public void findLongestPath() {
		
	}
	
	/**
	 * Divise la zone en deux parties lorsque possible, sinon, en crée une autre
	 * Sans cette fonction, le donjon ne serait qu'une seule salle immense	 
	 * @param dungeon : le donjon qui sera manipulé
	 * @param zone : la zone qui sera éditée
	 * @param seed : valeur aléatoire influant des éléments du donjon
	 */
	private void generate(Dungeon dungeon, Zone zone, Random seed) {
		if (shouldSplit(zone, seed)) {
			if (zone.getWidth() > zone.getHeight()) {
				splitVertical(dungeon, zone, seed);
			} else {
				splitHorizontal(dungeon, zone, seed);
			}
		} else {
          createRoom(dungeon, zone, seed);
		}
	}
	
	/**
	 * Vérifie qu'une zone peut être divisée en deux.
	 * @param zone : la zone qui sera divisée.
	 * @param seed : élément aléatoire influant sur la création de la salle.
	 * @return booléenne : indique si la zone peut être divisée (true) ou pas (false).
	 */
	private boolean shouldSplit(Zone zone, Random seed) {
		return zone.getWidth() > MIN_ROOM_SIZE * 2 + 1 || zone.getHeight() > MIN_ROOM_SIZE * 2 + 1;
	}
	
  /**
   * Pour créer une salle en enlevant des murs.
   * @param dungeon : le donjon qui sera manipulé.
   * @param zone : la zone concernée par la transformation
   * @param seed : valeur aléatoire influant des éléments du donjon
   */
	private void createRoom(Dungeon dungeon, Zone zone, Random seed) {
		Zone room = zone.reduce(1, 1);
		fill(dungeon, room, MapElt.EMPTY);
	}

	/**
	 * Construit le donjon à l'aide des Enum
	 * @param dungeon : le donjon concerné
	 * @param zone : la zone/"terrain de jeu" qui sera transformé(e)
	 * @param elt : les Enums servant à la construction du donjon
	 */
	private void fill(Dungeon dungeon, Zone zone, MapElt elt) {
		for (int y = zone.getStartY(); y <= zone.getEndY(); y++) {
			for (int x = zone.getStartX(); x <= zone.getEndX(); x++) {
				dungeon.set(x, y, elt);
			}
		}
	}
  
	/**
	 * Fait apparaître des monstres dans le donjon.
	 * @param dungeon : le donjon qui sera peuplé de monstres.
	 * @param seed : élément aléatoire influant sur la génération des monstres
	 * @author Cécile
	 */
	private void spawnMonsters(Dungeon dungeon, Random seed) {
		for (Zone monsterRoom : monsterRooms) {

			if (seed.nextFloat() < .3f) {
				int xx = monsterRoom.getStartX() + 3;
				int yy = monsterRoom.getStartY() + 3;

				if (!dungeon.get(xx, yy).isSolid()) {
					dungeon.setNumb(xx, yy, MapElt.MONSTER);
				}
			}
		}
	}
	
	/**
	 * Fait apparaître des items dans le donjon.
	 * @param dungeon : le donjon qui recevra les items.
	 * @param zone : la zone qui recevra lesitems.
	 * @param seed : élément aléatoire influant sur la génération des items.
	 * @author Cécile
	 */
	private void spawnItems(Dungeon dungeon, Zone room, Random seed) {
		for (Zone monsterRoom : monsterRooms) {
			if (seed.nextFloat() < .2f) {
				int xx = monsterRoom.getStartX() + 2;
				int yy = monsterRoom.getStartY() + 1;
				dungeon.set(xx, yy, MapElt.GOLD);
			}
		}
		if (seed.nextFloat() < .1f) {
			dungeon.set(rndRoomX(room, seed), rndRoomY(room, seed), MapElt.HEALTH);
		}
		if (seed.nextFloat() < .1f) {
			dungeon.set(rndRoomX(room, seed), rndRoomY(room, seed), MapElt.STRENGTH);
		}
	}
 
  private int rndRoomX(Zone room, Random seed) {
      return nextInt(seed, room.getStartX(), room.getEndX() + 1);
  }

	private int rndRoomY(Zone room, Random seed) {
		return nextInt(seed, room.getStartY(), room.getEndY() + 1);
	}
  
	// Return a random value
	private int nextInt(Random seed, int min, int max) {
		return seed.nextInt(max - min) + min;
	}
  
	private int random(int min, int max) {
		Random rand = new Random();
		return rand.nextInt(max - min) + min;
	}
 

	private void splitVertical(Dungeon dungeon, Zone zone, Random seed) {
		int v = nextInt(seed, zone.getStartX() + MIN_ROOM_SIZE + 1, zone.getEndX() - MIN_ROOM_SIZE);
		for (Zone child : zone.splitVertical(v)) {
			generate(dungeon, child, seed);
		}
		addVerticalDoor(dungeon, zone, seed, v);
	}


  private void addVerticalDoor(Dungeon dungeon, Zone zone, Random seed, int v) {
	  List<Integer> countV = new ArrayList<Integer>();
	  int y = 0;
	  do {
	    y = nextInt(seed, zone.getStartY(), zone.getEndY());
	    countV.add(y);
	  } while (dungeon.get(v + 1, y).isSolid() || dungeon.get(v - 1, y).isSolid());
	  for (int i = 0; i < countV.size(); i += 2) {
		  dungeon.set(v, y, MapElt.DOORV);
		  roomsList.add(zone);
	  }
	  for (int i = 1; i < countV.size(); i += 2) {
		  dungeon.set(v, y, MapElt.EMPTY);
		  roomsList.add(zone);
	  }
  }  
    
  private void splitHorizontal(Dungeon dungeon, Zone zone, Random seed) {
	  int h = nextInt(seed, zone.getStartY() + MIN_ROOM_SIZE + 1, zone.getEndY() - MIN_ROOM_SIZE);
	  for (Zone child : zone.splitHorizontal(h)) {
	    generate(dungeon, child, seed);
	  }
	  addHorizontalDoor(dungeon, zone, seed, h);
  }
	
	private void addHorizontalDoor(Dungeon dungeon, Zone zone, Random seed, int h) {
		List<Integer> countH = new ArrayList<Integer>();
		int x = 0;
		do {
			x = nextInt(seed, zone.getStartX(), zone.getEndX());
			countH.add(x);
		} while (dungeon.get(x, h + 1).isSolid() || dungeon.get(x, h - 1).isSolid());
		for (int i = 0; i < countH.size(); i++) {
			dungeon.set(x, h, MapElt.DOORH);
			roomsList.add(zone);
		}
		for (int i = 1; i < countH.size(); i++) {
			dungeon.set(x, h, MapElt.EMPTY);
			roomsList.add(zone);
		}
	}

  /** 
   * Méthode permettant de trouver les salles fermées
   * @todo améliorer la méthode
   * @param dungeon : le donjon du jeu.
   * @author Cécile
   */
  private void findClosedRooms(Dungeon dungeon) {
	  
	  for (Zone room : roomsList) {
		  boolean isClosed = true;
		  
		  for (int x = room.getStartX(); x <= room.getEndX(); x++) {
			  if (!dungeon.get(x, room.getStartY()).isSolid()) {
				  isClosed = false;
			/*	  dungeon.set(x, room.getStartY(), MapElt.TOP);
			  }else {
				  dungeon.set(x, room.getStartY(), MapElt.MISSING);*/
			  }
			  if (!dungeon.get(x, room.getEndY()).isSolid()){
				  isClosed = false;
			/*	  dungeon.set(x, room.getEndY(), MapElt.BOTTOM);
			  }else {
				  dungeon.set(x, room.getEndY(), MapElt.MISSING);*/
			  }
		  }
		  
		  for (int y = room.getStartY(); y <= room.getEndY(); y++) {
			  if (!dungeon.get(room.getStartX(), y).isSolid()) {
				  isClosed = false;/*
				  dungeon.set(room.getStartX(), y,MapElt.LEFT);
			  }else {
				  dungeon.set(room.getStartX(), y,MapElt.MISSING);*/
			  }
			  if(!dungeon.get(room.getEndX(), y).isSolid()) {
				  isClosed = false;/*
				  dungeon.set(room.getEndX(), y, MapElt.RIGHT);
			  }else {
				  dungeon.set(room.getEndX(), y, MapElt.MISSING);*/
			  }
		  }
		  if (isClosed) {
			  monsterRooms.add(room);
		  }
	  }
  }
}