package textDungeon.items;

import java.util.Scanner;

import textDungeon.actor.*;

/**
 * Classe de l'objet "potion de force". Hérite de la classe Item et de l'interface
 * ItemUse.
 * 
 * @author Mohamed
 */
public class StrenghtPotion extends Item implements ItemUse {

	public StrenghtPotion() {
		super('S');
		this.name = "potion de force";
	}

	/**
	 * Permet d'utiliser la potion de force.
	 * 
	 * @param player : le joueur qui utilisera la potion de force.
	 * @return isUsed : permet de dire si l'item a bien été utilisé.
	 * @author Mohamed
	 */
	public boolean use(Player player) {

		System.out.println("Voulez-vous utiliser la potion de force ?");
		System.out.println("A. Oui");
		System.out.println("B. Non");
		boolean isUsed = false;
		Scanner in = new Scanner(System.in);
		String reponse = "";
		do {
			reponse = in.next();
			if ("A".equalsIgnoreCase(reponse)) {
				player.setStrengthPoints(player.getStrengthPoints() + 5);
				System.out.println("Vous avez gagné 5 points de Force");
				isUsed = true;
				break;

			} else if ("B".equalsIgnoreCase(reponse)) {
				System.out.println("Vous n'avez pas utilisé la potion de force.");
				break;
			} else {
				System.out.println("Commande invalide, veuillez rééssayer");
				break;
			}
		} while (!reponse.equalsIgnoreCase("A") || !reponse.equalsIgnoreCase("B"));
		return isUsed;
	}

}
