package textDungeon.shionnDungeon;

import java.util.HashMap;

import textDungeon.actor.Monster;
import textDungeon.actor.Player;
import textDungeon.items.Item;

/** 
* Class generating the zone
* @author Shionn (http://www.shionn.org/)
*/
public class Zone {
	
	private int x;
	private int y;
	private int sx;
	private int ex;
	private int sy;
	private int ey;
	private int id;
	private int bit;
	private boolean visited;
	private HashMap <Integer, Monster> ListMonsters = new HashMap <Integer, Monster> () ;
	private HashMap <Integer, Item> ListItems= new HashMap <Integer, Item> () ;
	private HashMap <Integer, Player> ListPlayer= new HashMap <Integer, Player> () ;
	
	
	public HashMap<Integer, Item> getListItems() {
		return ListItems;
	}

	public void setListItems(int key, Item i) {
		ListItems.put(key,i);
	}
	
	public void removeItems (int key, HashMap <Integer, Item> ListItems) {
		ListItems.remove(key);
		
	}


	public HashMap<Integer, Player> getListPlayer() {
		return ListPlayer;
	}

	public void setListPlayer(int key, Player p) {
		ListPlayer.put(key,p);
	}
	
	public void removePlayer (int key, HashMap <Integer, Player> ListPlayer) {
		ListPlayer.remove(key);
		
	}

	public HashMap<Integer, Monster> getListMonsters() {
		return ListMonsters;
	}
	
	public void setListMonsters(int key, Monster m) {
		ListMonsters.put(key,m);
	}
	
	public void removeMonsters (int key, HashMap <Integer, Monster> ListMonsters) {
		ListMonsters.remove(key);
		
	}



	private boolean exit;
	public static int count = 0;
  
  //Zone constructor to build each room of the dungeon
	public Zone(int startx, int endx, int starty, int endy) {
		this.id = ++count;
	    this.sx = startx;
	    this.ex = endx;
	    this.sy = starty;
	    this.ey = endy;
	}
	
	public boolean isExit() {
		return exit;
	}

	public void setExit(boolean exit) {
		this.exit = exit;
	}

	public Zone (int x, int y) {
		this.id = ++count;
		this.x = x;
		this.y = y;
	}
  
  //Used to balance the coordinates so it does not fall out of bound
  public Zone reduce(int x, int y) {
      return new Zone(sx + x, ex - x, sy + y, ey - y);
  }

  //Split the zone in two, through the h line
  public Zone[] splitHorizontal(int h) {
      return new Zone[] { new Zone(sx, ex, sy, h), new Zone(sx, ex, h, ey) };
  }

  //Split the zone in two, through the v column
  public Zone[] splitVertical(int v) {
      return new Zone[] { new Zone(sx, v, sy, ey), new Zone(v, ex, sy, ey) };
  }
  
  //Getters and setters

  public int getHeight() {
	  return ey - sy;
  }
  public int getWidth() { 
	  return ex - sx; 
  }
  public int getStartX() { 
	  return sx;
  }
  public void setStartX(int x) {
	  this.sx = x;
  }
  public int getX() {
	  return x;
  }
  
  public int getEndX() { 
	  return ex; 
  }
  
  public int getStartY() { 
	  return sy;
  }
  public void setStartY(int y) {
	  this.sy = y;
  }
  public int getY() {
	  return y;
  }
  
  public int getEndY() { 
	  return ey;
  }
  
  public boolean isVisited() {
	  return visited;
  }
  public void setVisited(Boolean bool) {
	  this.visited = bool;
  }
  
  public int getId() {
		return id;
  }
  
  public int getBit() {
		return bit;
  }
}