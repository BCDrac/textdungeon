package textDungeon.items;

import textDungeon.actor.Player;

/**
 * Interface des items
 * 
 * @author Cécile
 */
public interface ItemUse {
	
	/**
	 * Permet à l'objet d'être utilisé par le joueur
	 * @param player : le joueur qui utilisera l'objet
	 */
	public boolean use(Player player);

	/**
	 * A été créé pour gérer l'affichage de l'icône, n'a pour le moment pas d'utilisation.
	 * Statut incertain, probable dépréciation.
	 */
	public void disappear();

}
