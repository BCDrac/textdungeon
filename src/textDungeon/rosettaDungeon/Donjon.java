package textDungeon.rosettaDungeon;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Classe Donjon permettant de générer et gérer le donjon du jeu.
 * @author rosettacode.org
 *
 */
public class Donjon {
	String javaPathValue = System.getenv().get("DUNGEON_PATH");
	ResourceBundle rb = ResourceBundle.getBundle(javaPathValue);
	private int x;
	private int y;
	private int maxX;
	private int maxY;
	private int minX;
	private int minY;
	private Salle[][] donjon;
	private final int[][] maze;
	private LinkedList<Salle> listSalles;
	private int end;
	

	public Donjon() {

		this.x = Integer.parseInt(rb.getString("heightDungeon"));
		this.y = Integer.parseInt(rb.getString("widthDungeon"));
		
		this.minX = 0;
		this.maxX = this.x;
		this.maxY = 0;
		
		this.maxY = this.y;
		donjon = new Salle[this.x][this.y];
		maze = new int[this.x][this.y];
		end = this.x * this.y - 1;
		generateAndCheck(0, 0);
	}

	public void displayEnd() {
		System.out.println("La sortie est à l'id " + this.end);
	}

	/**
	 *  Génération du labyrinthe
	 * @param cx : coordonnée x du donjon
	 * @param cy : coordonnée y du donjon
	 */
	public void generateMaze(int cx, int cy) {
		DIR[] dirs = DIR.values();

		Collections.shuffle(Arrays.asList(dirs));

		for (DIR dir : dirs) {

			int nx = cx + dir.dx;
			int ny = cy + dir.dy;

			if (between(nx, x) && between(ny, y) && (donjon[nx][ny] == null)) {

				if (donjon[cx][cy] == null) {
					donjon[cx][cy] = new Salle(dir.bit, cx, cy);
					addDoorOrEmptyDoor(cx, cy);
				} else {
					donjon[cx][cy].setBit(donjon[cx][cy].getBit() + dir.bit);
					addDoorOrEmptyDoor(cx, cy);
				}

				if (donjon[nx][ny] == null) {
					donjon[nx][ny] = new Salle(dir.opposite.bit, nx, ny);
					addDoorOrEmptyDoor(nx, ny);
				} else {
					donjon[nx][ny].setBit(donjon[nx][ny].getBit() + dir.opposite.bit);
					addDoorOrEmptyDoor(nx, ny);
				}
				generateMaze(nx, ny);
			}
		}
		// Inititalisation de la salle d'entrée.
		donjon[0][0] = new SalleEntree();
	}

	/**
	 *  fonction récursive servant a regénerer un tableau si la sortie n'est pas
	 *  positionnée sur un coin du donjon.
	 * @param cx : coordonnée x du donjon
	 * @param cy : coordonnée y du donjon
	 */
	public void generateAndCheck(int cx, int cy) {
		generateMaze(cx, cy);
		if (checkLastSalle()) {
			return;
		} else {
			this.donjon = new Salle[this.x][this.y];
			Salle.setCount(10);
			this.end = this.x * this.y - 1;
			generateAndCheck(cx, cy);
		}
	}

	/**
	 * Permet de trouver le chemin le plus long depuis la position de départ
	 * et d'y mettre l'indice de sortie
	 */
	public void findLongestPath() {
		int compteur = 0;
		int x = 0;
		int y = 0;
		Set<Salle> compteurSalles = new HashSet<Salle>();
		LinkedList<Salle> listSalles = new LinkedList<Salle>();
		Map<Integer, Integer> nodes = new HashMap<Integer, Integer>();
		listSalles.push(donjon[x][y]);
		int indice = donjon[x][y].getId();
		indice++;
		while ((compteurSalles.size() != this.x * this.y - 1)) {
			do {
				// down
				if (y + 1 < this.y && donjon[x][y + 1].isVisited() == false && donjon[x][y + 1].getId() == indice) {
					listSalles.push(donjon[x][y + 1]);
					compteurSalles.add(donjon[x][y + 1]);
					donjon[x][y + 1].setVisited(true);
					compteur++;
					y++;
					indice++;
				}
				// left
				else if (x + 1 < this.x && donjon[x + 1][y].isVisited() == false
						&& donjon[x + 1][y].getId() == indice) {
					x++;
					listSalles.push(donjon[x][y]);
					compteurSalles.add(donjon[x][y]);
					donjon[x][y].setVisited(true);
					compteur++;
					indice++;
				}
				// up
				else if (y - 1 >= 0 && donjon[x][y - 1].isVisited() == false && donjon[x][y - 1].getId() == indice) {
					y--;
					listSalles.push(donjon[x][y]);
					compteurSalles.add(donjon[x][y]);
					donjon[x][y].setVisited(true);
					compteur++;
					indice++;
				}
				// right
				else if (x - 1 >= 0 && donjon[x - 1][y].isVisited() == false && donjon[x - 1][y].getId() == indice) {
					x--;
					listSalles.push(donjon[x][y]);
					compteurSalles.add(donjon[x][y]);
					donjon[x][y].setVisited(true);
					compteur++;
					indice++;
				} else {
					nodes.put(listSalles.size() + 1, indice - 1);
					break;
				}

			} while (compteurSalles.size() != this.x * this.y - 1);

			if (listSalles.size() != 0) {
				listSalles.removeFirst();
				x = listSalles.getFirst().getPositionX();
				y = listSalles.getFirst().getPositionY();
			}
		}
		nodes.put(listSalles.size() + 1, indice - 1);

		int maxValue = Integer.MIN_VALUE;
		int endIndice = 0;
		Set<Entry<Integer, Integer>> setHm = nodes.entrySet();
		Iterator<Entry<Integer, Integer>> it = setHm.iterator();
		while (it.hasNext()) {
			Entry<Integer, Integer> e = it.next();
			for (Map.Entry<Integer, Integer> entry : nodes.entrySet()) {
				if (entry.getKey() >= maxValue) {
					maxValue = entry.getKey();
					endIndice = entry.getValue();
				}
			}
		}
		this.end = endIndice;
	}

	private static boolean between(int v, int upper) {
		return (v >= 0) && (v < upper);
	}

	/**
	 * Enum servant à se repérer dans le donjon
	 *
	 */
	private enum DIR {
		N(1, 0, -1), S(2, 0, 1), E(4, 1, 0), W(8, -1, 0);

		private final int bit;
		private final int dx;
		private final int dy;
		private DIR opposite;

		// use the static initializer to resolve forward references
		static {
			N.opposite = S;
			S.opposite = N;
			E.opposite = W;
			W.opposite = E;
		}

		private DIR(int bit, int dx, int dy) {
			this.bit = bit;
			this.dx = dx;
			this.dy = dy;
		}
	};

	/**
	 * Methode servant a définir les portes
	 * @param x : coordonnée x du donjon
	 * @param y : coordonnée y du donjon
	 */
	public void addDoorOrEmptyDoor(int x, int y) {
		if ((donjon[x][y].getBit() & 1) == 0) {
			donjon[x][y].setDoorN("+---");
			donjon[x][y].setdN(true);
		} else {
			donjon[x][y].setDoorN("+   ");
			donjon[x][y].setdN(false);
		}

		if ((donjon[x][y].getBit() & 2) == 0) {
			donjon[x][y].setDoorS("+---");
			donjon[x][y].setdS(true);
		} else {
			donjon[x][y].setDoorS("p    ");
			donjon[x][y].setdS(false);
		}

		if ((donjon[x][y].getBit() & 8) == 0) {
			donjon[x][y].setDoorW("|   ");
			donjon[x][y].setdW(true);
		} else {
			donjon[x][y].setDoorW("    ");
			donjon[x][y].setdW(false);
		}

		if ((donjon[x][y].getBit() & 4) == 0) {
			donjon[x][y].setDoorE("|");
			donjon[x][y].setdE(true);
		} else {
			donjon[x][y].setDoorE(" ");
			donjon[x][y].setdE(false);
		}
	}

	/**
	 * Méthode servant a refaire le tableau si le plus long chemin n'est pas collé
	 * aux contours du labyrinthe
	 */
	public boolean checkLastSalle() {
		findLongestPath();
		boolean check = false;
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {

				if (donjon[i][j].getId() == this.end) {
					if (((donjon[i][j].getPositionX() == 0 && donjon[i][j].getPositionX() < this.x))
							|| ((donjon[i][j].getPositionY() == 0 && donjon[i][j].getPositionY() < this.y))) {
						check = true;
						donjon[i][j].setExit(true);
						break;
					} else {
						check = false;
						break;
					}
				}
			}
		}
		return check;
	}
// -----------------------------------------METHODES D'AFFICHAGE LOG--------------------------------------------------------------------------------------------------------------------	

	/**
	 * Affichage donjon avec coordonnées de Salle
	 */
	public void display() {
		for (int i = 0; i < y; i++) {
			// draw the north edge

			for (int j = 0; j < x; j++) {
				System.out.print(donjon[j][i].getDoorN());
			}
			System.out.println("+");
			// draw the west edge
			for (int j = 0; j < x; j++) {
				if (getDonjon()[j][i].getListPlayer().isEmpty() != true) {
					if (getDonjon()[j][i].isdW() == true) {
						System.out.print((donjon[j][i].getBit() & 1) == 0 ? "| P " : "| P ");
					} else {
						System.out.print((donjon[j][i].getBit() & 1) == 0 ? "  P " : "  P ");
					}

				} else if (j == 0 && i == 0) {
					System.out.print((donjon[j][i].getBit() & 1) == 0 ? "    " : "+   ");
				} else if (donjon[j][i].getId() == this.end) {
					donjon[j][i].setExit(true);
					if ((donjon[j][i].getBit() & 8) == 0) {
						System.out.print("| O ");
					} else {
						System.out.print(" O ");
					}

				} else {
					System.out.print(donjon[j][i].getDoorW());
				}
			}
			System.out.println("|");
		}
		// draw the bottom line
		for (int j = 0; j < x; j++) {
			System.out.print("+---");
		}
		System.out.println("+");
	}

	/**
	 *  Affichage du donjon avec coordonnées de Salle
	 */
	public void displayCoordonnee() {
		for (int i = 0; i < y; i++) {
			// draw the north edge

			for (int j = 0; j < x; j++) {

				System.out.print((donjon[j][i].getDoorN() + donjon[j][i].getPositionX() + donjon[j][i].getPositionY()));
			}
			System.out.println("+");
			// draw the west edge
			for (int j = 0; j < x; j++) {
				if (j == 0 && i == 0) {
					System.out.print((donjon[j][i].getBit() & 1) == 0
							? "P   " + donjon[j][i].getPositionX() + donjon[j][i].getPositionY()
							: "+   ");
				} else if (donjon[j][i].getId() == this.end) {
					donjon[j][i].setExit(true);
					if ((donjon[j][i].getBit() & 8) == 0) {
						System.out.print("|O  ");
					} else {
						System.out.print(" O  ");
					}

				} else {
					System.out.print((donjon[j][i].getDoorW())/* )+donjon[j][i].getId() */ + donjon[j][i].getPositionX()
							+ donjon[j][i].getPositionY());
				}
			}
			System.out.println("|");
		}
		// draw the bottom line
		for (int j = 0; j < x; j++) {
			System.out.print("+---");
		}
		System.out.println("+");
	}

	/**
	 *  Affichage du donjon avec toutes les portes
	 */
	public void displayDoors() {
		for (int i = 0; i < y; i++) {
			// draw the north edge

			for (int j = 0; j < x; j++) {
				System.out.print(donjon[j][i].getDoorN());
			}
			System.out.println("+");
			// draw the west edge
			for (int j = 0; j < x; j++) {
				if (j == 0 && i == 0) {
					System.out.print((donjon[j][i].getBit() & 1) == 0 ? "P   " : "+   ");

				} else if(donjon[j][i].getId() == this.end){
					donjon[j][i].setExit(true);
					if((donjon[j][i].getBit() & 8) == 0) {
						
						System.out.print("| O " );
					} else {
						System.out.print(" O  " );
					}
						
				} else if (donjon[j][i].getId() == this.end) {
					donjon[j][i].setExit(true);
					if ((donjon[j][i].getBit() & 8) == 0) {
						System.out.print("|O ");
					} else {
						System.out.print(" O ");
					}

				} else {
					System.out.print(donjon[j][i].getDoorW());
					System.out.print(donjon[j][i].getDoorE());
				}
			}
			System.out.println("L");
			for (int j = 0; j < x; j++) {

				System.out.print(donjon[j][i].getDoorS());
			}
			System.out.println("+");
		}
		// draw the bottom line
		for (int j = 0; j < x; j++) {
			System.out.print("+---");
		}
		System.out.println("+");
	}

	/**
	 *  Affichage d'un tableau de données int du donjon
	 */
	public void displayValeurBit() {
		for (int i = 0; i < y; i++) {
			// draw the north edge
			for (int j = 0; j < x; j++) {
				System.out.print((donjon[j][i].getBit() & 1) == 0 ? "+" + (donjon[j][i].getBit() & 1) + "--"
						: "+" + (donjon[j][i].getBit() & 1) + "  ");
			}
			System.out.println("+");
			// draw the west edge
			for (int j = 0; j < x; j++) {
				System.out.print((donjon[j][i].getBit() & 8) == 0 ? " |" + (donjon[j][i].getBit() & 8) + "  "
						: "" + (donjon[j][i].getBit() & 8) + "  ");
			}
			System.out.println("|");
		}
		// draw the bottom line
		for (int j = 0; j < x; j++) {
			System.out.print("+---");
		}
		System.out.println("+");
	}

	public void displaySalleBit() {
		for (int i = 0; i < y; i++) {
			// draw the north edge
			for (int j = 0; j < x; j++) {
				System.out.print("+ " + donjon[j][i].getBit());
			}
			System.out.println("+");
			// draw the west edge
		}
	}

	public void displayMazeBit() {
		for (int i = 0; i < x; i++) {
			// draw the north edge
			for (int j = 0; j < y; j++) {
				System.out.print("+ " + maze[i][j]);
			}
			System.out.println("+"); // draw the west edge
		}
	}

	private int nextInt(Random seed, int min, int max) {
		return seed.nextInt(max - min) + min;
	}

	private int random(int min, int max) {
		Random rand = new Random();
		return rand.nextInt(max - min) + min;
	}

	public Salle[][] getDonjon() {
		return donjon;
	}

	public void setDonjon(Salle[][] donjon) {
		this.donjon = donjon;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getMinX() {
		return minX;
	}

	public int getMaxX() {
		return maxX;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getMinY() {
		return minY;
	}

	public int getMaxY() {
		return maxY;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	// ----------------------------------------- Methode Main utilisée pour la
	// compréhension et la création du
	// labyrinthe-------------------------------------------------
//	public static void main(String[] args) {
//		int x = args.length >= 1 ? (Integer.parseInt(args[0])) : 8;
//		int y = args.length == 2 ? (Integer.parseInt(args[1])) : 8;
//		Donjon donjon = new Donjon();
//		donjon.generateMaze(0, 0);
//		//donjon.generateAndCheck();
//		donjon.display();
//		donjon.displayValeurBit();
//		donjon.displaySalleBit();
//	}
}