package textDungeon.actor;

/**
 * Classe Monster permettant de générer les monstres.
 * Hérite de la classe Character.
 * @author Mohamed
 */
public class Monster extends Character {

	protected String name;
	protected char icon;
	protected int number;

	/**
	 * Constructeur de la classe Monster
	 * 
	 * @param name : le nom du monstre
	 * @param totalHP : le nombre total de points de vie
	 * @param strengthPoints : la force du monstre, en points de dégâts
	 * @param amountOfGold : la quantité d'or que le monstre possède
	 * @param currentHP : les points de vie actuels du monstre
	 * @author Mohamed
	 */
	public Monster(String name, int totalHP, int strengthPoints, int amountOfGold, int currentHP) {
		super(totalHP, strengthPoints, amountOfGold, currentHP);
		this.name = name;
		this.icon = 'M';
	}

	/**
	 * Contre-attaque du monstre.
	 * @param player : le joueur qui subira la contre-attaque.
	 * @author Mohamed
	 */
	public void hitBack(Player player) {
		if (this.isAttacked) {
			player.setCurrentHP(player.getCurrentHP() - this.strengthPoints);
			this.checkHP();
			System.out.println(
					"Le " + this.name + " riposte ! Il vous a infligé " + this.strengthPoints + " points de dégâts.");
			System.out.println("Il vous reste " + player.currentHP + " HP.\n");
			if (player.currentHP <= 0) {
				player.die();
			}
		}
	}

	/**
	 * Affichage de la mort du monstre et du gain de son or.
	 * 
	 * @author Mohamed
	 */
	public void showDie() {
		if (this.getCurrentHP() <= 0) {
			System.out.println("Le " + this.name + " ennemi a été tué ! Vous gagnez " + this.amountOfGold + " pièces d'or !");
		}
	}

	// Getters and setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString () {
		return " Le " + this.name + " ennemi possede " + this.currentHP + " / " + this.totalHP + " HP, "
				+ this.strengthPoints + " points de Force et " + this.amountOfGold + " pieces d'Or.";
	}

}
