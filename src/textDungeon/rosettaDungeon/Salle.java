package textDungeon.rosettaDungeon;

import java.util.HashMap;

import textDungeon.actor.Monster;
import textDungeon.actor.Player;
import textDungeon.items.Item;

/**
 * Classe Salle permettant de créer les différentes salles du donjon.
 * 
 * @author rosettacode.org
 *
 */
public class Salle {
	private int id;
	private int bit;
	private int positionX;
	private int positionY;

	private boolean isVisited;
	private String doorN;
	private boolean dN;
	private String doorW;
	private boolean dW;
	private String doorS;
	private boolean dS;
	private String doorE;
	private boolean dE;
	private boolean isExit;
	
	private HashMap<Integer, Monster> ListMonsters = new HashMap<Integer, Monster>();
	private HashMap<Integer, Item> ListItems = new HashMap<Integer, Item>();
	private HashMap<Integer, Player> ListPlayer = new HashMap<Integer, Player>();

	public static int count = 10;

	public Salle(int bit, int positionX, int positionY) {
		this.id = ++count;
		this.bit = bit;
		this.positionX = positionX;
		this.positionY = positionY;
	}

	public Salle(int bit, int positionX, int positionY, String doorW) {
		this.id = ++count;
		this.bit = bit;
		this.positionX = positionX;
		this.positionY = positionY;
		this.doorW = doorW;
	}

	public Salle(int id, int bit, int positionX, int positionY, String doorW, String doorN, boolean dW, boolean dN) {
		this.id = id;
		this.bit = bit;
		this.positionX = positionX;
		this.positionY = positionY;
		this.doorW = doorW;
		this.doorN = doorN;
		this.dW = dW;
		this.dN = dN;
	}

	@Override
	public String toString() {
		return "La salle contient : " + ListMonsters.size() + " monstre(s) et " + ListItems.size() + " objet(s)";
	}

	public boolean isdN() {
		return dN;
	}
	
	public void setdN(boolean dN) {
		this.dN = dN;
	}

	public boolean isdW() {
		return dW;
	}
	public void setdW(boolean dW) {
		this.dW = dW;
	}

	public String getDoorN() {
		return doorN;
	}
	public void setDoorN(String doorN) {
		this.doorN = doorN;
	}

	public String getDoorW() {
		return doorW;
	}
	public void setDoorW(String doorW) {
		this.doorW = doorW;
	}

	public boolean isdS() {
		return dS;
	}
	public void setdS(boolean dS) {
		this.dS = dS;
	}

	public String getDoorS() {
		return doorS;
	}
	public void setDoorS(String doorS) {
		this.doorS = doorS;
	}

	public String getDoorE() {
		return doorE;
	}
	public void setDoorE(String doorE) {
		this.doorE = doorE;
	}

	public boolean isdE() {
		return dE;
	}
	public void setdE(boolean dE) {
		this.dE = dE;
	}

	public int getId() {
		return id;
	}

	public int getBit() {
		return bit;
	}
	public void setBit(int bit) {
		this.bit = bit;
	}

	public int getPositionX() {
		return positionX;
	}
	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}
	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

	public boolean isExit() {
		return isExit;
	}
	public void setExit(boolean isExit) {
		this.isExit = isExit;
	}

	public boolean isVisited() {
		return isVisited;
	}
	public void setVisited(boolean isVisited) {
		this.isVisited = isVisited;
	}

	public static int getCount() {
		return count;
	}
	public static void setCount(int count) {
		Salle.count = count;
	}

	public void getPositions() {
		System.out.println("x :" + positionX + ", y :" + positionY);
	}

	public HashMap<Integer, Item> getListItems() {
		return ListItems;
	}
	public void setListItems(int key, Item i) {
		ListItems.put(key, i);
	}

	public void removeItems(int key, HashMap<Integer, Item> ListItems) {
		ListItems.remove(key);
	}

	public HashMap<Integer, Player> getListPlayer() {
		return ListPlayer;
	}
	public void setListPlayer(int key, Player p) {
		ListPlayer.put(key, p);
	}
	public void removePlayer(int key, HashMap<Integer, Player> ListPlayer) {
		ListPlayer.remove(key);
	}

	public HashMap<Integer, Monster> getListMonsters() {
		return ListMonsters;
	}
	public void setListMonsters(int key, Monster m) {
		ListMonsters.put(key, m);
	}

	public void removeMonsters(int key, HashMap<Integer, Monster> ListMonsters) {
		ListMonsters.remove(key);
	}

}