package textDungeon.items;

import java.util.Random;
import java.util.Scanner;

import textDungeon.actor.Player;

/**
 * Classe du bandit manchot. Hérite de la classe Item et de l'interface ItemUse
 * 
 * @author Mohamed
 */
public class OneArmedBandit extends Item implements ItemUse {

	public OneArmedBandit() {
		super('$');
		this.name = "bandit manchot";
	}

	/**
	 * Permet d'utiliser le bandit manchot pour obtenir des items
	 * 
	 * @param player : le joueur qui utilisera le bandit manchot
	 * @author Mohamed
	 */
	public boolean use(Player player) {
		int goldLost = randInt(100, 200);
		System.out.println("C'est un Bandit Manchot, vous pouvez lui donner " + goldLost + " pièces d'or. Voulez-vous l'utiliser ?");
		System.out.println("A. Oui");
		System.out.println("B. Non");
		boolean isUsed = false;
		Scanner in = new Scanner(System.in);
		String reponse = "";
		do {
			reponse = in.next();

			// Ajouter un RandInt pour l'or ainsi qu'un objet aléatoire au lieu de la force
			int goldWin = randInt(150, 300);
			int strenghtWin = randInt(3, 12);
			int healthWin = randInt(50, 100);
			if ("A".equalsIgnoreCase(reponse) && player.getAmountOfGold() >= goldLost) {

				player.setAmountOfGold(player.getAmountOfGold() - goldLost);
				int randItems = randInt(1, 3);
				if (randItems == 1) {

					StrenghtPotion strenghtpotion = new StrenghtPotion();
					System.out.println("Vous avez récupéré une potion de Force !");
					player.setStrengthPoints(player.getStrengthPoints() + strenghtWin);
					System.out.println("Vous avez donné " + goldLost + " pièces d'or et gagne " + strenghtWin
							+ " points de Force.");
					isUsed = true;
					break;
				}

				if (randItems == 2) {
					HealthPotion healthpotion = new HealthPotion();
					System.out.println("Vous avez récupéré une potion de Soin !");
					player.setCurrentHP(player.getCurrentHP() + healthWin);
					if (player.getCurrentHP() > player.getTotalHP()) {
						player.setCurrentHP(player.getTotalHP());
						healthWin = player.getTotalHP() - player.getCurrentHP();
					}
					System.out.println(
							"Vous avez donné " + goldLost + " pièces d'or et gagné " + healthWin + " points de Vie.");
					isUsed = true;
					break;
				}

				if (randItems == 3) {
					GoldBag goldbag = new GoldBag();
					System.out.println("Vous avez récuperé une bourse d'Or !");
					player.setAmountOfGold(player.getAmountOfGold() + goldWin);
					System.out.println(
							"Vous avez donné " + goldLost + " pièces d'or et gagné " + goldWin + " pièces d'or.");
					isUsed = true;
					break;
				}

			} else if ("B".equalsIgnoreCase(reponse)) {
				System.out.println("Vous n'avez pas utilisé le Bandit Manchot.");
				break;
			}

			else if (player.getAmountOfGold() < goldLost) {
				System.out.println("Vous n'avez pas assez d'or pour utiliser le Bandit Manchot ! \nLe Bandit Manchot disparaît !");
				isUsed = true;
				break;
			} else {
				System.out.println("Commande invalide, veuillez rééssayer !");
				break;
			}
		} while (!reponse.equalsIgnoreCase("A") || !reponse.equalsIgnoreCase("B"));
		return isUsed;
	}

	public static int randInt(int min, int max) {
		if (min >= max) {
			System.out.println("La valeur max doit être supérieure à la valeur min.");
		}
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
}
