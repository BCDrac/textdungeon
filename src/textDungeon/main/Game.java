package textDungeon.main;

import java.util.Random;
import java.util.ResourceBundle;
import java.util.Scanner;

import textDungeon.actor.Monster;
import textDungeon.actor.Player;
import textDungeon.items.*;
import textDungeon.rosettaDungeon.Donjon;

/**
 * Classe d'affichage du jeu et des commandes possibles.
 * 
 * @author Cécile
 *
 */
public class Game {

	/**
	 * Affichage du menu du jeu ainsi que du donjon.
	 * 
	 * @author Mohamed, édité par Cécile
	 */
	public static void displayMenu() {
		int x = 0;
		int y = 0;

		String javaPathValue = System.getenv().get("DUNGEON_PATH");
		ResourceBundle rb = ResourceBundle.getBundle(javaPathValue);
		
		Donjon donjon = new Donjon();
		Player player = new Player(Integer.parseInt(rb.getString("hpPlayer")), Integer.parseInt(rb.getString("strenghtPlayer")), Integer.parseInt(rb.getString("goldPlayer")), 500, "Mohamed");
		donjon.getDonjon()[0][0].getListPlayer().put(100, player);
		createMonsters(donjon, 10);
		createItems(donjon, 10);

		donjon.getDonjon()[0][0].setdW(true);

		if (donjon.getDonjon()[0][1].isdN()) {
			donjon.getDonjon()[0][0].setdS(true);
		}
		if (donjon.getDonjon()[1][0].isdW()) {
			donjon.getDonjon()[0][0].setdE(true);
		}

		String reponse = "";
		Scanner in = new Scanner(System.in);
		do {
			donjon.display();

			String menu = "\n";
			menu += ("-------------------- TEXT DUNGEON --------------------\n");
			menu += ("Que voulez-vous faire ?\n");
			menu += ("1 - Se déplacer\n");
			menu += ("2 - Attaquer\n");
			menu += ("3 - Se reposer\n");
			menu += ("4 - Ramasser un objet\n");
			menu += ("5 - Visualiser la carte\n");
			menu += ("6 - Quitter\n");
			menu += ("------------------------------------------------------\n");
			System.out.println(menu);
			reponse = in.nextLine();

			switch (reponse) {
			case "1":
				if (!donjon.getDonjon()[x][y].getListMonsters().isEmpty()) {
					System.out.println("Des monstres vous barrent le passage !");
				} else {
					System.out.println("Où souhaitez-vous vous déplacer ?");
					showMoves(donjon, x, y);

					String rep = in.nextLine().toUpperCase();
					player.move(rep, x, y, donjon);

					if (x < donjon.getMaxX() - 1 && !donjon.getDonjon()[x + 1][y].getListPlayer().isEmpty()) {
						x++;
					}
					if (x > donjon.getMinX() && !donjon.getDonjon()[x - 1][y].getListPlayer().isEmpty()) {
						x--;
					}
					if (y < donjon.getMaxY() - 1 && !donjon.getDonjon()[x][y + 1].getListPlayer().isEmpty()) {
						y++;
					}
					if (y > donjon.getMinY() && !donjon.getDonjon()[x][y - 1].getListPlayer().isEmpty()) {
						y--;
					}

					// Check if the player is on an exit tile to show victory
					checkExitForVictory(donjon, x, y, player);
				}
				break;

			case "2":
				if (donjon.getDonjon()[x][y].getListMonsters().isEmpty()) {
					System.out.println("Il n'y a pas de monstre à attaquer dans la pièce.");

				} else {

					for (int a : donjon.getDonjon()[x][y].getListMonsters().keySet()) {
						System.out.println("La clé du monstre ennemi est : " + a + "."
								+ donjon.getDonjon()[x][y].getListMonsters().get(a));
					}
					System.out.println("Veuillez entrez la clé du monstre que vous voulez attaquer : ");
					int keyMonster = in.nextInt();
					if (donjon.getDonjon()[x][y].getListMonsters().containsKey(keyMonster)) {
						player.attack(donjon.getDonjon()[x][y].getListMonsters().get(keyMonster));

						if (donjon.getDonjon()[x][y].getListMonsters().get(keyMonster).getCurrentHP() <= 0) {
							donjon.getDonjon()[x][y].getListMonsters().remove(keyMonster);
						}
					}
					in.nextLine();
				}
				break;

			case "3":
				if (donjon.getDonjon()[x][y].getListMonsters().isEmpty()
						&& player.getCurrentHP() < player.getTotalHP()) {
					player.healHP(20);

					if (player.getCurrentHP() > player.getTotalHP()) {
						player.setCurrentHP(player.getTotalHP());
					}
					System.out.println("Vous avez regagné 20 points de vie !");
				}

				else if (donjon.getDonjon()[x][y].getListMonsters().isEmpty() == false) {
					System.out.println("Vous devez éliminer les monstres restants avant de pouvoir vous reposer !");

				} else if (player.getCurrentHP() == player.getTotalHP()) {
					System.out.println("Vos points de vie sont déjà au maximum !");
				}
				break;

			case "4":
				if (donjon.getDonjon()[x][y].getListItems().isEmpty()) {
					System.out.println("Il n'y a pas d'objets dans la pièce.");

				} else {

					for (int a : donjon.getDonjon()[x][y].getListItems().keySet()) {
						System.out.println(
								"La clé de l'objet est : " + a + "." + donjon.getDonjon()[x][y].getListItems().get(a));
					}
					System.out.println("Veuillez entrez la clé de l'objet que vous voulez utiliser : ");
					int keyItem = in.nextInt();
					if (donjon.getDonjon()[x][y].getListItems().containsKey(keyItem)) {
						if (donjon.getDonjon()[x][y].getListItems().get(keyItem).use(player)) {
							donjon.getDonjon()[x][y].getListItems().get(keyItem).disappear();
							donjon.getDonjon()[x][y].getListItems().remove(keyItem);
						}
					}
				}
				break;

			case "5":
				System.out.println("Où souhaitez-vous regarder ?");
				System.out.println("A - Salle actuelle");

				if (!donjon.getDonjon()[x][y].isdE() && donjon.getDonjon()[x][y].getListMonsters().isEmpty()) {
					System.out.println("B - A droite");
				}

				if (!donjon.getDonjon()[x][y].isdW() && donjon.getDonjon()[x][y].getListMonsters().isEmpty()) {
					System.out.println("C - A gauche");
				}

				if (!donjon.getDonjon()[x][y].isdN() && donjon.getDonjon()[x][y].getListMonsters().isEmpty()) {
					System.out.println("D - En haut");
				}

				if (!donjon.getDonjon()[x][y].isdS() && donjon.getDonjon()[x][y].getListMonsters().isEmpty()) {
					System.out.println("E - En bas");
				}

				String rep = in.nextLine().toUpperCase();
				player.lookAround(rep, x, y, donjon.getDonjon());
				break;

			case "6":
				System.out.println("À bientôt pour une prochaine partie !");
				System.exit(-1);
				break;

			default:
				System.out.println("Commande invalide, veuillez réessayer");
				break;
			}
		} while (!reponse.equals("6"));
	}

	/**
	 * Méthode pour générer les monstres qui peupleront le donjon
	 * 
	 * @param donjon     : le donjon du jeu
	 * @param nbMonsters : le nombre de monstres générés par la méthode
	 * @author Mohamed, édité par Cécile
	 */
	public static void createMonsters(Donjon donjon, int nbMonsters) {

		for (int i = 0; i < nbMonsters; i++) {
			int randMonsters = randInt(0, 100);
			if (randMonsters <= 50) {
				donjon.getDonjon()[randInt(0, 4)][randInt(0, 4)].getListMonsters().put(i,
						new Monster("Gobelin", 150, 10, 40, 150));
			}

			if (randMonsters > 50 && randMonsters <= 90) {
				donjon.getDonjon()[randInt(0, 4)][randInt(0, 4)].getListMonsters().put(i,
						new Monster("Squelette", 250, 15, 80, 250));
			}

			if (randMonsters > 90) {
				donjon.getDonjon()[randInt(0, 4)][randInt(0, 4)].getListMonsters().put(i,
						new Monster("Dragon", 500, 30, 200, 500));
			}
		}
	}

	/**
	 * Méthode pour générer les items qui se trouveront dans le donjon
	 * 
	 * @param donjon  : le donjon du jeu
	 * @param nbItems : le nombre d'items générés par la méthode
	 * @author Mohamed, édité par Cécile
	 */
	public static void createItems(Donjon donjon, int nbItems) {

		for (int i = 0; i < nbItems; i++) {
			int randItems = randInt(0, 4);
			if (randItems == 1) {
				donjon.getDonjon()[randInt(0, 4)][randInt(0, 4)].getListItems().put(i, new StrenghtPotion());
			} else if (randItems == 2) {
				donjon.getDonjon()[randInt(0, 4)][randInt(0, 4)].getListItems().put(i, new HealthPotion());
			} else if (randItems == 3) {
				donjon.getDonjon()[randInt(0, 4)][randInt(0, 4)].getListItems().put(i, new GoldBag());
			} else if (randItems == 4) {
				donjon.getDonjon()[randInt(0, 4)][randInt(0, 4)].getListItems().put(i, new OneArmedBandit());
			}
		}
	}

	/**
	 * Méthode pour montrer les déplacements disponibles au joueur
	 * 
	 * @param donjon : le donjon du jeu
	 * @param x : coordonnée x du donjon
	 * @param y : coordonnée y du donjon
	 * @author Mohamed, édité par Cécile
	 */
	public static void showMoves(Donjon donjon, int x, int y) {
		if (donjon.getDonjon()[x][y].getPositionX() < donjon.getMaxX() && !donjon.getDonjon()[x][y].isdE()
				&& donjon.getDonjon()[x][y].getListMonsters().isEmpty()) {
			System.out.println("A - A droite");
		}

		if (donjon.getDonjon()[x][y].getPositionX() > donjon.getMinX() && !donjon.getDonjon()[x][y].isdW()
				&& donjon.getDonjon()[x][y].getListMonsters().isEmpty()) {
			System.out.println("B - A gauche");
		}

		if (donjon.getDonjon()[x][y].getPositionY() > donjon.getMinY() && !donjon.getDonjon()[x][y].isdN()
				&& donjon.getDonjon()[x][y].getListMonsters().isEmpty()) {
			System.out.println("C - En haut");
		}

		if (donjon.getDonjon()[x][y].getPositionY() < donjon.getMaxY() && !donjon.getDonjon()[x][y].isdS()
				&& donjon.getDonjon()[x][y].getListMonsters().isEmpty()) {
			System.out.println("D - En bas");
		}
	}

	/**
	 * Méthode pour vérifier que le joueur se trouve sur la sortie, afficher le
	 * message de victoire et quitter le jeu
	 * 
	 * @param donjon : le donjon du jeu
	 * @param x      : coordonnée x du donjon
	 * @param y      : coordonnée y du donjon
	 * @param player : le joueur
	 * @author Cécile
	 */
	public static void checkExitForVictory(Donjon donjon, int x, int y, Player player) {
		if (donjon.getDonjon()[x][y].isExit() == true) {
			System.out.println(
					"Félicitations, vous avez terminé le donjon avec " + player.getAmountOfGold() + " pièces d'Or !");
			System.exit(-1);
		}
	}

	public static int randInt(int min, int max) {
		if (min >= max) {
			System.out.println("La valeur max doit être supérieure à la valeur min");
		}
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
}

// Quels sont les objets a développer ?
// Quels sont les composants et les composés ?
// Quels sont les services de chaque objet ?
// Quels sont les objets prioritaires à développer ?