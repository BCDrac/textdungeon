------------------
---TEXT DUNGEON---
------------------

Bin�me compos� de : Mohamed MEDDAH - C�cile BAILLARD


-----------------------
-------Objectifs-------
-----------------------

Cr�er une application de type jeu vid�o appartenant au genre rogue-like/dungeon crawler.
Le projet requiert la manipulation de tableaux, collections, recursivit�, conditions/boucles et la prise en charge de fichiers.


-----------------------
-----�l�ments requis---
-----------------------

Ce projet prend en compte un fichier de type properties (config.properties) afin d'initialiser les �l�ments suivants :

heightDungeon = la taille y du donjon
widthtDungeon = la taille x du donjon
hpPlayer = les points de vie du personnage joueur
strenghtPlayer = la force (points de d�g�ts) du personnage joueur
goldPlayer = la quantit� d'or du joueur

Ce fichier n�cessite la variable d'environnement syst�me suivante :
Variable : DUNGEON_PATH
Valeur : properties.config

-----------------------
-------Structure-------
-----------------------

Le project est compos� des 6 packages :

textDungeon.main : contient la classe Main ainsi que la classe Game qui elle g�n�re la session de jeu.

textDungeon.actor : contient les personnages qui parcourent le donjon (joueur et monstres). Les classes Player et Monster h�ritent de la classe-m�re Character.
textDungeon.items : contient les objets utilisables par le joueur et pouvant �tre trouv�s dans le donjon : GoldBag, HealthPotion, OneArmedBandit et StrengthPotion. Ces classes h�ritent de la classe abstraite Items qui impl�mente l'interface ItemUse.

textDungeon.rosettaDungeon, textDungeon.shionnDungeon et textDungeon.customDungeon : ils contiennent les algorithmes de g�n�ration de donjon, textDungeon.rosettaDungeon est celui qui sert de base au projet, textDungeon.shionnDungeon �tant le premier algorithme utilis� et textDungeon.customDungeon n'�tant qu'une vaine tentative de recr�er un algorithme personnel.

-----------------------
---------D�buts--------
-----------------------

La premi�re chose � faire fut de rassembler les informations pour savoir comment organiser le projet : quelles classes utiliser, comment les organiser, les fonctions � pr�voir...
Le syst�me de versioning Git ainsi qu'une interface graphique, Git Kraken, allaient servir � g�rer le projet et partager les modifications.
La seconde chose � faire �tait de trouver ce qui allait �tre la base du projet : un algorithme permettant de g�n�rer un donjon.

Deux algorithmes furent retenus :

Le "donjon de Shionn" (http://www.shionn.org/algorihtme-donjon-aleatoire).
Ce donjon utilise les �num�rations (Enum), un type de valeur constante, pour cr�er son aspect visuel � l'aide de l'ELT (Extract-Load-Transform), un principe de transfert et d'utilisation de donn�es.
Le "donjon de Shionn" pr�parait un tableau servait de terrain pour le donjon, le remplissait de char servant de murs puis creusait les salles au fur et � mesure, divisant en deux celles qui pouvaient l'�tre et ajoutant des portes.
Une grande partie du temps fut consacr�e � comprendre le fonctionnement de cet agorithme qui fut, un moment, le principal g�n�rateur de donjon du projet.

Le "donjon de Rosetta" (https://rosettacode.org/wiki/Maze_generation#Java)
Ce donjon se dessine salle par salle avec des Strings, ajoutant des charact�res suivant la position du pointeur dans le tableau.
L'utilisation des Enums dans ce donjon servait principalement pour l'orientation lors de la construction du terrain, permettant au code d'adapter son affichage suivant les donn�es du tableau.
Cet algorithme sert de g�n�rateur de donjon pour ce projet.


-----------------------
-Premi�res difficult�s-
-----------------------

Il va s'en dire qu'analyser un algorithme de g�n�ration de donjon n'est pas chose ais�e pour un novice.
C�cile BAILLARD d�cida de s'occuper de l'algorithme du donjon. Le "donjon de Shionn" fut choisi pour sa fa�on d'utiliser les Enum qui permettaient une g�n�ration pr�cise ainsi que ses aspects nouveaux qui offraient un certain d�fi.
N�anmoins, il y eu une difficult� du c�t� de la manipulation de Listes, ce qui pourtant allait �tre un �l�ment principal du projet. Cette �tape s'av�ra chronophage et impacta n�gativement le reste des op�rations.
N'�tant pas totalement satisfaite du "donjon de Shionn", C�cile d�cida de tenter de cr�er son propre algorithme. Cette d�cision fut de courte dur�e compte tenu de la complexit� d'un tel algorithme au stade de connaissance actuel. N'�tant plus ou moins qu'un copi�-coll� du "donjon de Shionn" avec quelques modifications, cet embryon de donjon en est rest� � son stade pr�matur�.
Il fut finalement d�cid� d'utiliser le "donjon de Rosetta" comme support principal du projet.

Mohamed MEDDAH eu des difficult�s � comprendre la fa�on dont la g�n�ration de donjon se faisait. Il d� passer par plusieurs g�n�rateurs avant d'utiliser celui de Rosetta.
Il pu n�anmoins s'occuper de cr�er les classes principales et se basant sur le "donjon de Rosetta" et par-l� m�me acquit une avance qui lui permit de r�aliser une grande partie du projet.
Malgr� son travail, le fonctionnement du "donjon de Rosetta" reste un peu �nigmatique pour lui, l'�l�ment l'ayant le plus occup� �tant la m�thode permettant de se d�placer dans une salle d�j� visit�e en pr�sence de monstres.
L'organisation du code dans les classes lui est peu facile � cerner.
Le changement d'algorithme fut en revanche d�trimental pour C�cile (de Shionn � Rosetta) car n'ayant pas autant �tudi� tous les �l�ments de Rosetta pour en comprendre le fonctionnement.

R�sultat de cette �tape :
------------------------- 
Perte de temps provoqu�e par un acharnement sur un algorithme complexe (le "donjon de Shionn"), puis un projet d'algorithmie dont ni le temps allou�, ni les connaissances n'auraient permis son ach�vement.
Confusion et difficult�s dans la compr�hension des algorithmes de g�n�ration des donjons et dans l'utilisation des diff�rentes collections.


------------------------
----Le commit maudit----
------------------------

Apr�s ces premi�res difficult�s vint l'incident dit du "commit maudit", imputable � C�cile.
Ayant d� renommer des classes du projet, elle prit la d�cision de renommer �galement des packages et dossiers. L'�tape fut confuse et maladroite et engendra un �v�nement qui allait impacter encore une fois le projet.
Suite � cette manipulation, l'IDE Eclipse ne trouvait plus la classe Main du projet et les tentatives de r�tablir l'organisation empir�rent la situation (suppression des fichiers par Eclipse). Pour ajouter � la gravit� de la situation, le renommage de dossier fut push sur le repository bien trop h�tivement, et l'interface graphique Git Kraken ne faisait qu'afficher des erreurs lorsqu'il fut tent� de corriger le probl�me.
Ce n'est qu'apr�s une manipulation tout aussi maladroite (s�paration de la branche HEAD, commits, fusion du stash et rattachement de la branche HEAD � la branche principale) que la situation s'appaisa, non sans laisser des s�quelles pendant quelques commits (apparition de mots-cl�s sp�cifiques � git dans le code source du projet).

R�sultat de cette �tape :
------------------------- 
Aujourd'hui �tiquet� "CursedCommit" dans la branche principale, cet �v�nement sert de rappel aux �ventuelles cons�quences que peuvent avoir des modifications, commits et pushs imprudents et non r�fl�chis.


------------------------
--Retard et d�ception---
------------------------
Apr�s ces difficult�s et �v�nements infortun�s, force est de constater que le projet fut n�gativement impact� et ne peut malheureusement pas r�pondre � toutes les attentes du cahier des charges.
Son stade actuel est vu comme un �chec pour C�cile car caus� en majorit� par ses erreurs. Mohamed put faire en sorte que le projet arrive � un stade assez avanc� pour �tre livrable malgr� les obstacles rencontr�s.