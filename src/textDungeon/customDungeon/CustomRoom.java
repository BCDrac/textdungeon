package textDungeon.customDungeon;

/**
 * Classe permettant de créer des salles dans le donjon
* Non utilisé dans le projet actuel.
* @author Cécile, basé sur Shionn (http://www.shionn.org/)
*/
public class CustomRoom {
	
	private int x;
	private int y;
	private int sx;
	private int ex;
	private int sy;
	private int ey;
	private int id;
	private int bit;
	private boolean visited;
	public static int count = 0;
	
	private CustomMapElt[][] map;
	
	// Room constructor to build each room of the dungeon
	public CustomRoom (int startx, int endx, int starty, int endy) {
		this.map = new CustomMapElt[endx][endy];
		this.id = ++count;
		this.sx = startx;
		this.ex = endx;
		this.sy = starty;
		this.ey = endy;
	}

	public CustomRoom(int x, int y) {
		this.map = new CustomMapElt[x][y];
		this.id = ++count;
		this.x = x;
		this.y = y;
	}

	// Used to balance the coordinates so it does not fall out of bound
	public CustomRoom reduce(int x, int y) {
		return new CustomRoom(sx + x, ex - x, sy + y, ey - y);
	}
	
	public void set(int x, int y, CustomMapElt elt) { 
		map[x][y] = elt;
	}
	
	public int getStartX() {
		return sx;
	}
	
	public int getStartY() {
		return sy;
	}

	public int getEndX() {
		return ex;
	}
	
	public int getEndY() {
		return ey;
	}



}
