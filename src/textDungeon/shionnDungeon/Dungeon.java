package textDungeon.shionnDungeon;

import java.util.Random;

public class Dungeon {
	/** 
	* Class generating the dungeon
	* @author Shionn (http://www.shionn.org/)
	*/
	private Zone [][] dungeon;
private MapElt[][] maps; 
	
	public Dungeon(int w, int h) {
		this.dungeon = new Zone [w][h];
	    this.maps = new MapElt[w][h];
	}
	
	public void print() {
		for (int y = 0; y < getHeight(); y++) {
			for (int x = 0; x < getWidth(); x++) {
				System.out.print(maps[x][y].getGraphic());
			}
			System.out.println();
		}
	}
	
	public Zone[][] getDungeon() {
		return dungeon;
	}

	public void setDungeon(Zone[][] dungeon) {
		this.dungeon = dungeon;
	}

	public int getHeight() { 
		return maps[0].length; 
	}
	public int getWidth() { 
		return maps.length; 
	}
	public void set(int x, int y, MapElt elt) { 
		maps[x][y] = elt;
	}
	public void setNumb(int x, int y, MapElt elt) {
		MapElt numb = randomNumb();
		maps[x - 1][y] = numb;
		maps[x][y] = elt;
	}

	public MapElt get(int x, int y) { 
		return maps[x][y];
	}
	
	private MapElt[] numbers = new MapElt[] {MapElt.ONE, MapElt.TWO, MapElt.THREE};
	// this generates random numbers
	private Random random = new Random();
	// choose a card at random
	final MapElt randomNumb(){
	     return numbers[random.nextInt(numbers.length)];
	}

	public void set(int x, int h, MapElt doorh, MapElt doorh2) {
		// TODO Auto-generated method stub
		
	}

}