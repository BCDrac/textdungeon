package textDungeon.customDungeon;

import java.util.Random;

/** 
* Projet personnel pour générer un donjon, basé sur celui de Shionn.
* Non utilisé dans le projet actuel.
* @author Cécile, basé sur Shionn (http://www.shionn.org/)
*/
public class CustomDungeon {
	

private CustomMapElt[][] maps;
	
	/**
	 * Crée une nouvelle map (array) d'après la classe CustomMapElt
	 * @param w : la largeur du tableau
	 * @param h : la hauteur du tableau
	 */
	public CustomDungeon(int w, int h) {
	    this.maps = new CustomMapElt[w][h]; 
	}
	
	/**
	 * Affiche le donjon.
	 */
	public void print() {
		for (int y = 0; y < getHeight(); y++) {
			for (int x = 0; x < getWidth(); x++) {
				System.out.print(maps[x][y].getGraphic());
			}
			System.out.println();
		}
	}

	public int getHeight() { 
		return maps[0].length; 
	}
	public int getWidth() { 
		return maps.length; 
	}
	public void set(int x, int y, CustomMapElt elt) { 
		maps[x][y] = elt;
	}
	public void setNumb(int x, int y, CustomMapElt elt) {
		CustomMapElt numb = randomNumb();
		maps[x - 1][y] = numb;
		maps[x][y] = elt;
	}

	public CustomMapElt get(int x, int y) { 
		return maps[x][y];
	}
	
	private CustomMapElt[] numbers = new CustomMapElt[] {CustomMapElt.ONE, CustomMapElt.TWO, CustomMapElt.THREE};
	// this generates random numbers
	private Random random = new Random();
	// choose a card at random
	final CustomMapElt randomNumb(){
	     return numbers[random.nextInt(numbers.length)];
	}
	
}