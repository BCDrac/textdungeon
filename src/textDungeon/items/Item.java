package textDungeon.items;

import textDungeon.actor.Player;

/**
 * Classe abstraite des items
 * 
 * @author Cécile
 */
public abstract class Item implements ItemUse {

	protected String name;
	protected char icon;
	
	public Item(char icon) {
		this.icon = icon;
	}

	public boolean use(Player player) {
		return false;
	}
	
	/**
	 * A été créé pour gérer l'affichage de l'icône, n'a pour le moment pas d'utilisation.
	 * Statut incertain, probable dépréciation.
	 */
	public void disappear() {
		this.icon = ' ';
	}

	// Getters and setters

	public char getIcon() {
		return icon;
	}
	public void setIcon(char icon) {
		this.icon = icon;
	}
	public String getName() {
		return name;
	}
	
	public String toString () {
		return " L'objet est '" + this.name + "'";
	}

}
