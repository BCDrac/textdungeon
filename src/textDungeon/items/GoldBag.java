package textDungeon.items;

import java.util.Random;
import java.util.Scanner;

import textDungeon.actor.Player;

/**
 * Classe de l'item Sac d'or. Hérite de la classe Item et de l'interface ItemUse
 * 
 * @author Mohamed
 */
public class GoldBag extends Item implements ItemUse {

	public GoldBag() {
		super('G');
		this.name = "sac d'or";
	}

	Scanner in = new Scanner(System.in);

	/**
	 * Permet d'utiliser d'obtenir le sac d'or et d'augmenter son score de fin
	 * @param player : le joueur qui utilisera le sac d'or
	 * @return isUsed : permet de dire si l'item a bien été utilisé.
	 * @author Mohamed
	 */
	public boolean use(Player player) {
		System.out.println("Voulez-vous prendre le sac d'or ?");
		System.out.println("A. Oui");
		System.out.println("B. Non");
		boolean isUsed = false;
		String reponse = "";
		do {
			reponse = in.next();
			if (reponse.equalsIgnoreCase("A")) {
				int goldWin = randInt(20, 60);
				player.setAmountOfGold(player.getAmountOfGold() + goldWin);
				System.out.println("Vous avez gagné " + goldWin + " pièces d'Or.");
				isUsed = true;
				break;

			} else if (reponse.equalsIgnoreCase("B")) {
				System.out.println("Vous n'avez pas ramassé le sac d'or.");
				break;

			} else {
				System.out.println("Commande invalide, veuillez réessayer");
				break;
			}
		} while (!reponse.equalsIgnoreCase("A") || !reponse.equalsIgnoreCase("B"));
		return isUsed;
	}

	public static int randInt(int min, int max) {
		if (min >= max) {
			System.out.println("La valeur max doit être supérieure à la valeur min.");
		}
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

}
