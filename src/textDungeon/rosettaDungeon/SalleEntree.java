package textDungeon.rosettaDungeon;

/**
 * Permet de créer l'entrée du donjon
 *
 */
public class SalleEntree extends Salle{

	public SalleEntree(int bit, int positionX, int positionY) {
		super(bit, positionX, positionY);
	}
	
	public SalleEntree() {
		super(11,2, 0, 0, "> J ", "+---", false, true);
	}
	
}
