package textDungeon.actor;

/**
 * Classe-mère Character, a comme classes filles Player et Monster.
 * 
 * @author Mohamed
 */
public class Character {

	protected int totalHP;
	protected int currentHP;
	protected int strengthPoints;
	protected int amountOfGold;
	protected boolean isAttacked;

	/**
	 * Constructeur de la classe-mère Character
	 * 
	 * @param totalHP : le nombre total de points de vie du personnage
	 * @param strengthPoints : la force du personnage, en points de dégâts
	 * @param amountOfGold : la quantité d'or que possède le personnage
	 * @param currentHP : les points de vie actuels du personnage
	 * @author Mohamed
	 */
	public Character(int totalHP, int strengthPoints, int amountOfGold, int currentHP) {

		this.totalHP = totalHP;
		this.currentHP = currentHP;
		this.strengthPoints = strengthPoints;
		this.amountOfGold = amountOfGold;
	}

	/**
	 * Permet de vérifier les HP et éviter les négatifs
	 * 
	 * @author Cécile
	 */
	public void checkHP() {
		if (currentHP < 0) {
			setCurrentHP(0);
		}
	}

	// Getters and setters

	public int getCurrentHP() {
		return currentHP;
	}

	public void setCurrentHP(int currentHP) {
		this.currentHP = currentHP;
	}

	public int getTotalHP() {
		return totalHP;
	}

	public void setTotalHP(int totalHP) {
		this.totalHP = totalHP;
	}

	public int getStrengthPoints() {
		return strengthPoints;
	}

	public void setStrengthPoints(int strengthPoints) {
		this.strengthPoints = strengthPoints;
	}

	public int getAmountOfGold() {
		return amountOfGold;
	}

	public void setAmountOfGold(int amountOfGold) {
		this.amountOfGold = amountOfGold;
	}

	public boolean isAttacked() {
		return isAttacked;
	}

	public void setAttacked(boolean isAttacked) {
		this.isAttacked = isAttacked;
	}
}
