package textDungeon.customDungeon;

/**
* Duplicata de la classe permettant de créer des salles dans le donjon
* Non utilisé dans le projet actuel.
* @author Cécile, basé sur Shionn (http://www.shionn.org/)
*/
public class CustomZone {
	
	private int x;
	private int y;
	private int sx;
	private int ex;
	private int sy;
	private int ey;
	private int id;
	private int bit;
	private boolean visited;
	public static int count = 0;
	
private CustomMapElt[][] map;
	
  //Room constructor to build each room of the dungeon
	public CustomZone(int startx, int endx, int starty, int endy) {
		this.map = new CustomMapElt[endx][endy];
		this.id = ++count;
	    this.sx = startx;
	    this.ex = endx;
	    this.sy = starty;
	    this.ey = endy;
	}
	
	public CustomZone (int x, int y) {
		this.map = new CustomMapElt[x][y];
		this.id = ++count;
		this.x = x;
		this.y = y;
	}
	
	public CustomZone reduce(int x, int y) {
	      return new CustomZone(sx + x, ex - x, sy + y, ey - y);
	  }
  
	// Getters and setters
	public CustomMapElt get(int x, int y) {
		return map[x][y];
	}

	public int getHeight() {
		return ey - sy;
	}

	public int getWidth() {
		return ex - sx;
	}

	public int getStartX() {
		return sx;
	}

	public void setStartX(int x) {
		this.sx = x;
	}

	public int getX() {
		return x;
	}

	public int getEndX() {
		return ex;
	}

	public int getStartY() {
		return sy;
	}

	public void setStartY(int y) {
		this.sy = y;
	}

	public int getY() {
		return y;
	}

	public int getEndY() {
		return ey;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(Boolean bool) {
		this.visited = bool;
	}

	public int getId() {
		return id;
	}

	public int getBit() {
		return bit;
	}
}