package textDungeon.customDungeon;

import java.util.Random;

/** 
* Projet personnel pour générer un donjon, basé sur celui de Shionn.
* Générateur de donjon. Non utilisé dans le projet actuel.
* @author Cécile, basé sur Shionn (http://www.shionn.org/)
*/
public class CustomDungeonGenerator {
	
	private static final int MIN_ROOM_SIZE = 3;
	private static final int MIN = 12;
	private static final int MAX = 64;
	
	/**
	 * Génère un donjon en fonction d'une valeur aléatoire
	 * @param seed : une valeur aléatoire qui déterminera les caractéristiques du donjon.
	 * @return dungeon : renvoie le dungeon alors créé afin qu'il soit exploitable par d'autres classes.
	 */
	public CustomDungeon generate(Random seed) {
		CustomDungeon dungeon = new CustomDungeon(nextInt(seed, MIN, MAX), nextInt(seed, MIN, MAX));
		CustomZone zone = new CustomZone(0, dungeon.getWidth() - 1, 0, dungeon.getHeight() - 1);
		fill(dungeon, zone, CustomMapElt.WALL);
		generate(dungeon, zone);
		return dungeon;
	}

	// Return a random value
	private int nextInt(Random seed, int min, int max) {
		return seed.nextInt(max - min) + min;
	}

	private int random(int min, int max) {
		Random rand = new Random();
		return rand.nextInt(max - min) + min;
	}
	
	/**
	 * Remplit le donjon des char Enum.
	 * @param dungeon : le donjon qui sera construit.
	 * @param zone : zone interne du donjon qui sera construite.
	 * @param elt : les Enum qui eront utilisés pour la construction du donjon.
	 */
	private void fill(CustomDungeon dungeon, CustomZone zone, CustomMapElt elt) {
		for (int y = zone.getStartY(); y <= zone.getEndY(); y++) {
			for (int x = zone.getStartX(); x <= zone.getEndX(); x++) {
				dungeon.set(x, y, elt);
			}
		}
	}
	
	private void generate(CustomDungeon dungeon, CustomZone zone) {
		createRooms(dungeon, zone);
	}
	
	private void createRooms(CustomDungeon dungeon, CustomZone zone) {
		int zoneMaxX = 6;//random(MIN_ROOM_SIZE, 6);
		int zoneMaxY = 6;//random(MIN_ROOM_SIZE, 6);
		CustomRoom room = new CustomRoom(zone.getStartX() + 1, zoneMaxX, zone.getStartY() + 1, zoneMaxY);
		fillRoom(dungeon, room);
		/*boolean canBuild = false;
		for (int x = start; x < zoneMaxX; x++) {
			if (zone.get(x, start).isSolid()) {
				canBuild = true;
			}
		}	
		for (int y = start; y < zoneMaxY; y++) {
			if (zone.get(start, y).isSolid()) {
				canBuild = true;
			}
		}
		if (canBuild) {
			CustomRoom room = new CustomRoom(start + 1, zoneMaxX, start + 1, zoneMaxY);
			fillRoom(room);
		}*/
	}
	
	private void fillRoom(CustomDungeon dungeon, CustomRoom room) {
		for (int y = room.getStartY(); y < room.getEndY(); y++) {
			for (int x = room.getStartX(); x < room.getEndX(); x++) {
				dungeon.set(x, y, CustomMapElt.EMPTY);
			}
		}
	}

	
}
