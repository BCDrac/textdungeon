package textDungeon.customDungeon;

public enum CustomMapElt {
	/** Class allowing to create the map
	* @author Shionn (http://www.shionn.org/)
	*/
	
	/*
	*ELT : Extract, Load and Transform, a variant of ETL, is "a data manipulation process in database usage, especially in data warehousing"
	*"In ELT, data is extracted, loaded into the database and then is transformed where it sits in the database, prior to use."
	*In ELT, data is extracted, then loaded in the target system, once. This method is useful when there is a lot of data 
	*or when when the source and target databases are the same.
	*EXTRACT : extract the data from source. 
	*LOAD : deliver the data to where it will be used. ELT shorten the cycle between extraction and delivery.
	*TRANSFORM : transformation is performed in the staging area, an intermediate storage area used for data processing during ELT
	*STORAGE AREA : AKA "landing zone", a "transit zone" between the data source and data target. It can take the form of tables, XML files...
	*It's more or less a "simulated environment"
	*/
	
	WALL('#', true),
    EMPTY(' ', false),
    DOORV('|', true),
    DOORH('_', true),
    EXIT('E', true),
	PLAYER('P', false),
    MONSTER('M', false),
	GOLD('G', true),
	HEALTH('H', true),
	STRENGTH('S', true),
	TOP('^', true),
	BOTTOM ('v', true),
	LEFT('&', true),
	RIGHT('$', true),
	MISSING('@', false),
	ONE('1', false),
	TWO('2', false),
	THREE('3', false);
	
	/*
	*Enumeration : list of constants, without type. In Java, it's treated as a special class. Cannot be instanciated nor mixed with integers
	*Enum types implicitly extend the abstract class Enum
	*Each enum actually has, internally, an integer value depending of the order in which they have been declared
	*Each enum will have a character that act as a graphic representation 
	*/
	
	private char graphic;
	private boolean solid;
	
	//Map constructor, it takes the graphic char and solid boolean as arguments
	private CustomMapElt(char graphic, boolean solid) {
		this.graphic = graphic;
		this.solid = solid;
	}
	
	//---------
	
	public char getGraphic() {
		return graphic;
	}
	
	public boolean isSolid() {
		return solid;
	}
}