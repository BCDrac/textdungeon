package textDungeon.actor;

import java.util.Random;

import textDungeon.items.ItemUse;
import textDungeon.rosettaDungeon.Donjon;
import textDungeon.rosettaDungeon.Salle;

/**
 * Classe Player permettant de créer le joueur.
 * Hérite de la classe Character.
 * 
 * @author Mohamed, édité par Cécile
 */
public class Player extends Character {
	protected String name;

	public Player(int totalHP, int strenghtPoints, int amountOfGold, int currentHP, String name) {
		super(totalHP, strenghtPoints, amountOfGold, currentHP);
		this.name = name;
	}

	/**
	 * Fonction d'attaque.
	 * @param monster : le monstre qui sera attaqué par le joueur
	 * @author Mohamed
	 */
	public void attack(Monster monster) {
		monster.isAttacked = true;
		monster.setCurrentHP(monster.getCurrentHP() - this.getStrengthPoints());
		monster.checkHP();
		System.out.println(this.name + " attaque ! Le " + monster.getName() + " subit " + this.getStrengthPoints()
				+ " points de degats !");
		System.out.println("Il reste a votre adversaire " + monster.getCurrentHP() + " HP !");
		if (monster.getCurrentHP() <= 0) {
			this.kill(monster);
		} else {
			monster.hitBack(this);
		}
	}

	/**
	 * Afficher un Game over et stopper le jeu lorsque le heros meurt.
	 * 
	 * @author Mohamed
	 */
	public void die() {
		System.out.println("Vous avez perdu ! Le jeu est terminé.");
		System.exit(-1);
		// Fermer l'affichage du donjon et afficher un GAME OVER
	}

	/**
	 * Afficher la mort d'un monstre et empocher l'or.
	 * @param monster : le monstre qui doit mourrir.
	 * 
	 * @author Cecile
	 */
	public void kill(Monster monster) {
		monster.showDie();
		monster.isAttacked = false;
		this.amountOfGold += monster.getAmountOfGold();
	}

	/**
	 * Soigner les HP et éviter le surplus de vie.
	 * @param heal : la quantité de points de vie soignés.
	 * 
	 * @author Cecile
	 */
	public void healHP(int heal) {
		this.currentHP += heal;
		if (this.currentHP > this.totalHP) {
			this.currentHP = this.totalHP;
		}
	}

	/**
	 * Permet de se déplacer sur la carte
	 * @param answer : l'input du joueur
	 * @param x : coordonnée x du donjon 
	 * @param y : coordonnée y du donjon
	 * @param donjon : le donjon du jeu 
	 * @author Mohamed
	 */
	public void move(String answer, int x, int y, Donjon donjon) {
		switch (answer) {

		case "A":
			// switch Droite
			
			donjon.getDonjon()[x][y].removePlayer(100, donjon.getDonjon()[x][y].getListPlayer());
			donjon.getDonjon()[++x][y].setListPlayer(100, this);
			System.out.println("Vous avancez vers l'Est.\n");
			break;

		case "B":
			// switch Gauche
			donjon.getDonjon()[x][y].removePlayer(100, donjon.getDonjon()[x][y].getListPlayer());
			donjon.getDonjon()[--x][y].setListPlayer(100, this);
			System.out.println("Vous avancez vers l'Ouest.\n");
			break;

		case "C":
			// switch Haut
			donjon.getDonjon()[x][y].removePlayer(100, donjon.getDonjon()[x][y].getListPlayer());
			donjon.getDonjon()[x][--y].setListPlayer(100, this);
			System.out.println("Vous avancez vers le Nord.\n");
			break;

		case "D":
			// switch Bas
			donjon.getDonjon()[x][y].removePlayer(100, donjon.getDonjon()[x][y].getListPlayer());
			donjon.getDonjon()[x][++y].setListPlayer(100, this);
			System.out.println("Vous avancez vers le Sud.\n");
			break;

		default:
			System.out.println("Commande inconnue.");
			break;
		}
	}
	
	/**
	 * Visualiser des pièces du donjon
	 * @param answer : l'input du joueur
	 * @param x : coordonnée x du donjon 
	 * @param y : coordonnée y du donjon
	 * @param salles : les salles du donjon
	 * @author Mohamed
	 */
	public void lookAround(String answer, int x, int y, Salle[][] salles) {
		switch (answer) {
		
		case "A":
			// switch Salle actuelle
			System.out.println(salles[x][y]);
			System.out.println(this);
			break;

		case "B":
			// switch Droite
			System.out.println(salles[x+1][y]);
			break;

		case "C":
			// switch Gauche
			System.out.println(salles[x-1][y]);
			break;

		case "D":
			// switch Haut
			System.out.println(salles[x][y-1]);
			break;
			
		case "E":
			// switch Bas
			System.out.println(salles[x][y+1]);
			break;

		default:
			System.out.println("Commande inconnue.");
			break;
		}
	}

	@Override
	public String toString() {
		return "Le joueur " + this.name + " possède " + this.currentHP + " / " + this.totalHP + " HP, "
				+ this.strengthPoints + " points de Force et " + this.amountOfGold + " pièces d'or.";
	}

	public static int randInt(int min, int max) {
		if (min >= max) {
			System.out.println("La valeur max doit être supérieure à la valeur min.");
		}
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	// Getters and setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}