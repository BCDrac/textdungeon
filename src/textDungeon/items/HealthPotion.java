package textDungeon.items;

import java.util.Random;
import java.util.Scanner;

import textDungeon.actor.Player;

/**
 * Classe de l'objet "potion de vie" Hérite de la classe Item et de l'interface
 * ItemUse.
 * 
 * @author Mohamed
 */
public class HealthPotion extends Item implements ItemUse {

	public HealthPotion() {
		super('H');
		this.name = "potion de soin";
	}

	/**
	 * Permet d'utiliser la potion de soin.
	 * 
	 * @param player : le joueur qui utilisera la potion de soin.
	 * @return isUsed : booléenne, permet de dire si l'item a été utilisé
	 * @author Mohamed
	 */
	public boolean use(Player player) {
		System.out.println("Voulez-vous utiliser la potion de soin ?");
		System.out.println("A. Oui");
		System.out.println("B. Non");
		boolean isUsed = false;
		Scanner in = new Scanner(System.in);
		String reponse = "";
		do {
			reponse = in.next();
			if (reponse.equalsIgnoreCase("A") && player.getCurrentHP() != player.getTotalHP()) {

				int healthWin = randInt(30, 60);
				player.setCurrentHP(player.getCurrentHP() + healthWin);
				if (player.getCurrentHP() > player.getTotalHP()) {

					player.setCurrentHP(player.getTotalHP());
					healthWin = player.getTotalHP() - player.getCurrentHP();
				}
				System.out.println("Vous avez récuperé " + healthWin + " points de vie");
				isUsed = true;
				break;

			} else if (reponse.equalsIgnoreCase("B")) {
				System.out.println("Vous n'avez pas utilisé la potion de soin");
				break;

			} else if (player.getCurrentHP() == player.getTotalHP()) {
				System.out.println("Vos HP sont déjà au maximum !");
				break;

			} else {
				System.out.println("Commande invalide, veuillez rééssayer");
				break;
			}
		} while (!reponse.equalsIgnoreCase("A") || !reponse.equalsIgnoreCase("B"));
		return isUsed;
	}

	public static int randInt(int min, int max) {
		if (min >= max) {
			System.out.println("La valeur max doit être supérieure à la valeur min.");
		}
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

}
